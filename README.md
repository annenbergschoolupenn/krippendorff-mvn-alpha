# Software for calculating the alpha-reliability coefficient for multi-valued nominal data

## Dr Klaus Krippendorff - Annenberg School for Communication

### For instructions, please refer to user manual guide `User Guide.pdf`